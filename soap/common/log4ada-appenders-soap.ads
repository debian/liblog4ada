------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2013                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

with Ada.Strings.Unbounded;

package Log4ada.Appenders.SOAP is
   type Soap_Client_Type is new Appender_Type with private;
   procedure Append (Soap_Client : not null access Soap_Client_Type;
                     Event : Events.Event_Type);
   procedure Set_URL (Soap_Client : not null access Soap_Client_Type;
                      URL : String);
   procedure Set_ID (Soap_Client : not null access Soap_Client_Type;
                     ID : String);
   procedure Close (Soap_Client : not null access Soap_Client_Type);
private

   type Soap_Client_Type is new Appender_Type with record
      File_Log_Created : Boolean := False;
      URL : Ada.Strings.Unbounded.Unbounded_String :=
        Ada.Strings.Unbounded.To_Unbounded_String ("http://localhost:4242");
      ID : Ada.Strings.Unbounded.Unbounded_String :=
         Ada.Strings.Unbounded.To_Unbounded_String ("Misterious_ID");
   end record;

end Log4ada.Appenders.SOAP;
