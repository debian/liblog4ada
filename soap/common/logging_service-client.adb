
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

pragma Warnings (Off);

with SOAP.Client;
pragma Elaborate_All (SOAP.Client);
with SOAP.Message.Payload;
pragma Elaborate_All (SOAP);
pragma Elaborate_All (SOAP.Message);
pragma Elaborate_All (SOAP.Message.Payload);
with SOAP.Message.Response;
pragma Elaborate_All (SOAP.Message.Response);
with SOAP.Name_Space;
pragma Elaborate_All (SOAP.Name_Space);
with SOAP.Parameters;
pragma Elaborate_All (SOAP.Parameters);
with SOAP.Utils;
pragma Elaborate_All (SOAP.Utils);

package body Logging_Service.Client is

   use SOAP.Types;
   use type SOAP.Parameters.List;

   pragma Style_Checks (Off);

   ---------------------
   -- Create_File_Log --
   ---------------------

   procedure Create_File_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String)
   is
      P_Set   : SOAP.Parameters.List;
      Payload : SOAP.Message.Payload.Object;
   begin
      --  Set parameters
      P_Set := +SOAP.Types.S (Name_ID, "Name_ID");
      Payload := SOAP.Message.Payload.Build
        ("Create_File_Log", P_Set,
         SOAP.Name_Space.Create ("tns", "http://soapaws/Logging_def/"));

      declare
         Response : constant SOAP.Message.Response.Object'Class
           := SOAP.Client.Call
                (Connection, "Create_File_Log", Payload);
         R_Param  : constant SOAP.Parameters.List
           := SOAP.Message.Parameters (Response);
      begin
         if SOAP.Message.Response.Is_Error (Response) then
            raise SOAP.SOAP_Error with
               SOAP.Parameters.Get (R_Param, "faultstring");
         end if;
      end;
   end Create_File_Log;

   procedure Create_File_Log
     (Name_ID  : String;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts)
   is
      Connection : AWS.Client.HTTP_Connection;
   begin
      AWS.Client.Create
        (Connection, Endpoint,
         Persistent => False,
         Timeouts   => Timeouts);
      Create_File_Log (Connection, Name_ID);
      AWS.Client.Close (Connection);
   exception
      when others =>
         AWS.Client.Close (Connection);
         raise;
   end Create_File_Log;

   --------------------
   -- Close_File_Log --
   --------------------

   procedure Close_File_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String)
   is
      P_Set   : SOAP.Parameters.List;
      Payload : SOAP.Message.Payload.Object;
   begin
      --  Set parameters
      P_Set := +SOAP.Types.S (Name_ID, "Name_ID");
      Payload := SOAP.Message.Payload.Build
        ("Close_File_Log", P_Set,
         SOAP.Name_Space.Create ("tns", "http://soapaws/Logging_def/"));

      declare
         Response : constant SOAP.Message.Response.Object'Class
           := SOAP.Client.Call
                (Connection, "Close_File_Log", Payload);
         R_Param  : constant SOAP.Parameters.List
           := SOAP.Message.Parameters (Response);
      begin
         if SOAP.Message.Response.Is_Error (Response) then
            raise SOAP.SOAP_Error with
               SOAP.Parameters.Get (R_Param, "faultstring");
         end if;
      end;
   end Close_File_Log;

   procedure Close_File_Log
     (Name_ID  : String;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts)
   is
      Connection : AWS.Client.HTTP_Connection;
   begin
      AWS.Client.Create
        (Connection, Endpoint,
         Persistent => False,
         Timeouts   => Timeouts);
      Close_File_Log (Connection, Name_ID);
      AWS.Client.Close (Connection);
   exception
      when others =>
         AWS.Client.Close (Connection);
         raise;
   end Close_File_Log;

   ----------------
   -- Append_Log --
   ----------------

   procedure Append_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String;
      Message    : String;
      Level      : Level_Type_Type)
   is
      P_Set   : SOAP.Parameters.List;
      Payload : SOAP.Message.Payload.Object;
   begin
      --  Set parameters
      P_Set := +SOAP.Types.S (Name_ID, "Name_ID")
         & SOAP.Types.S (Message, "Message")
            & SOAP.Types.E (Types.Image (Level), "Level_Type", "Level");
      Payload := SOAP.Message.Payload.Build
        ("Append_Log", P_Set,
         SOAP.Name_Space.Create ("tns", "http://soapaws/Logging_def/"));

      declare
         Response : constant SOAP.Message.Response.Object'Class
           := SOAP.Client.Call
                (Connection, "Append_Log", Payload);
         R_Param  : constant SOAP.Parameters.List
           := SOAP.Message.Parameters (Response);
      begin
         if SOAP.Message.Response.Is_Error (Response) then
            raise SOAP.SOAP_Error with
               SOAP.Parameters.Get (R_Param, "faultstring");
         end if;
      end;
   end Append_Log;

   procedure Append_Log
     (Name_ID  : String;
      Message  : String;
      Level    : Level_Type_Type;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts)
   is
      Connection : AWS.Client.HTTP_Connection;
   begin
      AWS.Client.Create
        (Connection, Endpoint,
         Persistent => False,
         Timeouts   => Timeouts);
      Append_Log (Connection, Name_ID, Message, Level);
      AWS.Client.Close (Connection);
   exception
      when others =>
         AWS.Client.Close (Connection);
         raise;
   end Append_Log;

end Logging_Service.Client;
