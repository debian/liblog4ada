with Logging_Service.Client;

with Ada.Text_IO;

package body Log4ada.Appenders.SOAP is

   use Ada.Strings.Unbounded;

   procedure Append (Soap_Client : not null access Soap_Client_Type;
                     Event : Events.Event_Type) is
   begin
      Ada.Text_IO.Put_Line ("step 1 " & Soap_Client.File_Log_Created'Img);
      if not Soap_Client.File_Log_Created then
         Ada.Text_IO.Put_Line ("step 1 bis");
         Logging_Service.Client.Create_File_Log
           (To_String (Soap_Client.ID),
            Endpoint => To_String (Soap_Client.URL));
         Ada.Text_IO.Put_Line ("step 1 ter");
         Soap_Client.File_Log_Created := True;
      end if;
      Ada.Text_IO.Put_Line ("step 2 " & Soap_Client.File_Log_Created'Img);
      Logging_Service.Client.Append_Log
        (Name_ID => To_String (Soap_Client.ID),
         Message => Events.Get_Message (Event),
         Level => Events.Get_Level (Event),
         Endpoint => To_String (Soap_Client.URL));
      Ada.Text_IO.Put_Line ("step 3");
   end Append;

   procedure Set_URL (Soap_Client : not null access Soap_Client_Type;
                      URL : String) is
   begin
      Soap_Client.URL := To_Unbounded_String (URL);
   end Set_URL;

   procedure Set_ID (Soap_Client : not null access Soap_Client_Type;
                     ID : String) is
   begin
      Soap_Client.ID := To_Unbounded_String (ID);
   end Set_ID;

   procedure Close (Soap_Client : not null access Soap_Client_Type) is
   begin
      Logging_Service.Client.Close_File_Log
        (Name_ID => To_String (Soap_Client.ID),
         Endpoint => To_String (Soap_Client.URL));
   end Close;

end Log4ada.Appenders.SOAP;
