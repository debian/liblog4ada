
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

pragma Warnings (Off);

with Ada.Calendar;

with SOAP.Types;
pragma Elaborate_All (SOAP);
pragma Elaborate_All (SOAP.Types);

with Logging_Service.Types;

package Logging_Service.Client is

   use Logging_Service.Types;

   Connection : constant AWS.Client.HTTP_Connection;

   procedure Create_File_Log
     (Name_ID  : String;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts);
   procedure Create_File_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String);
   --  Raises SOAP.SOAP_Error if the procedure fails

   procedure Close_File_Log
     (Name_ID  : String;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts);
   procedure Close_File_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String);
   --  Raises SOAP.SOAP_Error if the procedure fails

   procedure Append_Log
     (Name_ID  : String;
      Message  : String;
      Level    : Level_Type_Type;
      Endpoint : String := Logging_Service.URL;
      Timeouts : AWS.Client.Timeouts_Values := Logging_Service.Timeouts);
   procedure Append_Log
     (Connection : AWS.Client.HTTP_Connection;
      Name_ID    : String;
      Message    : String;
      Level      : Level_Type_Type);
   --  Raises SOAP.SOAP_Error if the procedure fails

private

   Connection : constant AWS.Client.HTTP_Connection :=
                  AWS.Client.Create
                    (URL,
                     Timeouts   => Timeouts);

end Logging_Service.Client;
