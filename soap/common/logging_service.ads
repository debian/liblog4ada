
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

with AWS.Client;

package Logging_Service is

   URL      : constant String := "http://localhost:4242";
   Timeouts : constant AWS.Client.Timeouts_Values :=
                AWS.Client.No_Timeout;

end Logging_Service;
