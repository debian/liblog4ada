
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

with soapaws.Log4ada_pkg.Level_Type_Type_Pkg;
pragma Elaborate_All (soapaws.Log4ada_pkg.Level_Type_Type_Pkg);
use soapaws.Log4ada_pkg.Level_Type_Type_Pkg;

with Ada.Calendar;
with Ada.Strings.Unbounded;

with SOAP.Types;
pragma Elaborate_All (SOAP);
pragma Elaborate_All (SOAP.Types);
with SOAP.Utils;
pragma Elaborate_All (SOAP.Utils);

with Log4ada;
pragma Elaborate_All (Log4ada);

package Logging_Service.Types is

   pragma Warnings (Off, Ada.Calendar);
   pragma Warnings (Off, Ada.Strings.Unbounded);
   pragma Warnings (Off, SOAP.Types);
   pragma Warnings (Off, SOAP.Utils);
   pragma Warnings (Off, Log4ada);


   pragma Style_Checks (Off);

   use Ada.Strings.Unbounded;

   function "+"
     (Str : String)
      return Unbounded_String
      renames To_Unbounded_String;

   subtype Level_Type_Type is soapaws.Log4ada_pkg.Level_Type_Type_Pkg.Level_Type_Type;

   function Image (E : Level_Type_Type)
      return String
      renames soapaws.Log4ada_pkg.Level_Type_Type_Pkg.Image;

end Logging_Service.Types;
