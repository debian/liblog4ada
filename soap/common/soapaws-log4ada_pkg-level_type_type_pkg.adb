
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

with SOAP.Name_Space;
pragma Elaborate_All (SOAP);
pragma Elaborate_All (SOAP.Name_Space);

package body soapaws.Log4ada_pkg.Level_Type_Type_Pkg is

   pragma Warnings (Off, SOAP.Name_Space);

   use SOAP.Types;

   function Image (E : Level_Type_Type) return String is
   begin
      case E is
         when Log4ada.All_Level => return "All_Level";
         when Log4ada.Debug => return "Debug";
         when Log4ada.Info => return "Info";
         when Log4ada.Warn => return "Warn";
         when Log4ada.Error => return "Error";
         when Log4ada.Fatal => return "Fatal";
         when Log4ada.Off => return "Off";
      end case;
   end Image;

end soapaws.Log4ada_pkg.Level_Type_Type_Pkg;
