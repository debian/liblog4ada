------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Receiver_Dictionnary;
with Log4ada;
with Events_Receivers.Local_Consoles;
with Options;

procedure Init_Collector_Server is
   Local_Receiver : aliased
     Events_Receivers.Local_Consoles.Events_Display_Console_Type;
begin
   if Options.Present_Option ("local_file") then
      Events_Receivers.Local_Consoles.Initialise
        (Receiver => Local_Receiver,
         Level => Log4ada.Debug,
         Local_File => True,
         Local_File_Name => "default_file_name.txt",
         File_Size => 2048);
   else
      Events_Receivers.Local_Consoles.Initialise (Receiver => Local_Receiver,
                                                  Level => Log4ada.Debug);
   end if;

   Receiver_Dictionnary.Subscribe (Local_Receiver'Unchecked_Access);
   loop
      delay 100.0;
   end loop;
end Init_Collector_Server;
