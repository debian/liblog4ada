------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Receiver_Dictionnary;
with Events_Receivers;
with Log4ada.Loggers;
with Log4ada.Appenders.Annex_E;
with Common_Output;
with Options;

procedure Init_Test_Remote_Logging is
   Event_Receiver : Events_Receivers.Events_Receivers_Class_Access;
   Logger : aliased Log4ada.Loggers.Logger_Type;
   Appender : aliased Log4ada.Appenders.Annex_E.Remote_Appender_Type;
begin
   Log4ada.Loggers.Set_Name (Logger'Access, "joli logger");
   Log4ada.Loggers.Set_Level (Logger'Access, Log4ada.Debug);
   Event_Receiver := Receiver_Dictionnary.Get_Event_Receiver;
   Log4ada.Appenders.Annex_E.Set_Receiver (Appender, Event_Receiver);
   Log4ada.Loggers.Add_Appender (Logger'Access, Appender'Unchecked_Access);
   Common_Output (Logger'Access, not Options.Present_Option ("loop"));
end Init_Test_Remote_Logging;
