------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

with Log4ada.Events;

package body Events_Receivers.Local_Consoles is

   ----------------
   -- Send_Event --
   ----------------

   procedure Send_Event
     (Events_Receiver : not null access Events_Display_Console_Type;
      Event : String;
      Event_Type : Supported_Events_Type := Log4ada_Event)
   is
      Local_Event : Log4ada.Events.Event_Type;
   begin
      if Event_Type /= Log4ada_Event then
         raise Event_Type_Not_Supported;
      end if;
      Local_Event := Log4ada.Events.To_Event (Event);
      Log4ada.Loggers.Event_Out (Events_Receiver.Logger'Access, Local_Event);
   end Send_Event;

   procedure Initialise (Receiver : in out Events_Display_Console_Type;
                         Level : Log4ada.Level_Type;
                         Local_File : Boolean := False;
                         Local_File_Name : String := "local_log.txt";
                         File_Size : Log4ada.Appenders.Files.File_Size_Type :=
                           16384) is
   begin
      Log4ada.Loggers.Set_Level (Receiver.Logger'Access,
                                 Level);
      Log4ada.Loggers.Add_Appender (Receiver.Logger'Access,
                                    Receiver.Console'Unchecked_Access);
      if Local_File then
         Log4ada.Loggers.Add_Appender (Receiver.Logger'Access,
                                       Receiver.File'Unchecked_Access);
         Receiver.File.Set_File_Name (Local_File_Name);
         Receiver.File.Set_File_Max_Size (File_Size);
      end if;
   end Initialise;

end Events_Receivers.Local_Consoles;
