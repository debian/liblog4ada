------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

package Events_Receivers is
   pragma Remote_Types;
   type Supported_Events_Type is (Log4ada_Event, String_Event);
   type Events_Receiver_Type is abstract tagged limited private;
   type Events_Receivers_Class_Access is access all Events_Receiver_Type'Class;
   procedure Send_Event
     (Events_Receiver : not null access Events_Receiver_Type;
      Event : String;
      Event_Type : Supported_Events_Type := Log4ada_Event) is abstract;
   function Event_Receiver_Is_Alive
     (Events_Receiver : not null access Events_Receiver_Type) return Boolean;
   Event_Type_Not_Supported : exception;
private
   type Events_Receiver_Type is abstract tagged limited null record;
end Events_Receivers;
