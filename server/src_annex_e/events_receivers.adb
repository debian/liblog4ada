package body Events_Receivers is

   -----------------------------
   -- Event_Receiver_Is_Alive --
   -----------------------------

   function Event_Receiver_Is_Alive
     (Events_Receiver : not null access Events_Receiver_Type)
      return Boolean
   is
      pragma Unreferenced (Events_Receiver);
   begin
      return True;
   end Event_Receiver_Is_Alive;

end Events_Receivers;
