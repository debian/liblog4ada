------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Events_Receivers;

package Log4ada.Appenders.Annex_E is
   type Remote_Appender_Type is new Appender_Type with private;
   procedure Append (Remote_Appender : not null access Remote_Appender_Type;
                     Event : Events.Event_Type);
   procedure Set_Receiver
     (Remote_Appender : in out Remote_Appender_Type;
      Event_Receiver : Events_Receivers.Events_Receivers_Class_Access);
   No_Receiver : exception;
private
   type Remote_Appender_Type is new Appender_Type with record
      Event_Receiver : Events_Receivers.Events_Receivers_Class_Access := null;
   end record;
end Log4ada.Appenders.Annex_E;
