------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007-2009                          --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with System.RPC;

package body Log4ada.Appenders.Annex_E is

   ------------
   -- Append --
   ------------

   procedure Append
     (Remote_Appender : not null access Remote_Appender_Type;
      Event : Events.Event_Type)
   is
      use type Events_Receivers.Events_Receivers_Class_Access;
   begin
      if not Remote_Appender.Enabled then
         return;
      end if;
      if Remote_Appender.Event_Receiver = null then
         raise No_Receiver;
      end if;
      if not Continue (Remote_Appender, Event) then
         return;
      end if;
      declare
         Local_Receiver :
           constant Events_Receivers.Events_Receivers_Class_Access :=
           Remote_Appender.Event_Receiver;
      begin
         Events_Receivers.Send_Event (Local_Receiver,
                                      Events.To_String (Event),
                                      Events_Receivers.Log4ada_Event);
      exception
         when System.RPC.Communication_Error =>
            --  desabling remote logging
            Remote_Appender.Enabled := False;
            Remote_Appender.Event_Receiver := null;
      end;
   end Append;

   ------------------
   -- Set_Receiver --
   ------------------

   procedure Set_Receiver
     (Remote_Appender : in out Remote_Appender_Type;
      Event_Receiver : Events_Receivers.Events_Receivers_Class_Access)
   is
   begin
      Remote_Appender.Event_Receiver := Event_Receiver;
   end Set_Receiver;

end Log4ada.Appenders.Annex_E;
