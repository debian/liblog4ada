------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Log4ada.Appenders.Consoles;
with Log4ada.Appenders.Files;
with Log4ada.Loggers;

package Events_Receivers.Local_Consoles is
   type Events_Display_Console_Type is new Events_Receiver_Type with private;
   procedure Send_Event
     (Events_Receiver : not null access Events_Display_Console_Type;
      Event : String;
      Event_Type : Supported_Events_Type := Log4ada_Event);
   procedure Initialise (Receiver : in out Events_Display_Console_Type;
                         Level : Log4ada.Level_Type;
                         Local_File : Boolean := False;
                         Local_File_Name : String := "local_log.txt";
                         File_Size : Log4ada.Appenders.Files.File_Size_Type :=
                           16384);
private
   type Events_Display_Console_Type is new Events_Receiver_Type with record
      Logger : aliased Log4ada.Loggers.Logger_Type;
      Console : aliased Log4ada.Appenders.Consoles.Console_Type;
      File : aliased Log4ada.Appenders.Files.File_Type;
   end record;
end Events_Receivers.Local_Consoles;
