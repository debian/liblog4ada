
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

with AWS.Response;
with AWS.Status;

with SOAP.Dispatchers.Callback;
with SOAP.Message.Payload;

package Logging_Service.CB is

   use AWS;
   use SOAP;

   pragma Style_Checks (Off);

   subtype Handler is SOAP.Dispatchers.Callback.Handler;

   function SOAP_CB
     (SOAPAction : String;
      Payload    : Message.Payload.Object;
      Request    : AWS.Status.Data)
      return Response.Data;

end Logging_Service.CB;
