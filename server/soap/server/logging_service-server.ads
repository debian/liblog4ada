
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

pragma Warnings (Off);

with Ada.Calendar;

with AWS.Status;
pragma Elaborate_All (AWS.Status);
with AWS.Response;
pragma Elaborate_All (AWS.Response);

with SOAP.Message.Payload;
pragma Elaborate_All (SOAP);
pragma Elaborate_All (SOAP.Message);
pragma Elaborate_All (SOAP.Message.Payload);
with SOAP.Types;
pragma Elaborate_All (SOAP.Types);

with Logging_Service.Types;
pragma Elaborate_All (Logging_Service.Types);

package Logging_Service.Server is

   use Logging_Service.Types;

   Port : constant := 4242;

   generic
      with procedure Create_File_Log
        (Name_ID  : String);
   function Create_File_Log_CB
     (SOAPAction : String;
      Payload    : SOAP.Message.Payload.Object;
      Request    : AWS.Status.Data)
      return AWS.Response.Data;

   generic
      with procedure Close_File_Log
        (Name_ID  : String);
   function Close_File_Log_CB
     (SOAPAction : String;
      Payload    : SOAP.Message.Payload.Object;
      Request    : AWS.Status.Data)
      return AWS.Response.Data;

   generic
      with procedure Append_Log
        (Name_ID  : String;
         Message  : String;
         Level    : Level_Type_Type);
   function Append_Log_CB
     (SOAPAction : String;
      Payload    : SOAP.Message.Payload.Object;
      Request    : AWS.Status.Data)
      return AWS.Response.Data;

end Logging_Service.Server;
