
--  wsdl2aws SOAP Generator v2.3.0
--
--  AWS 3.1.0w - SOAP 1.5.0
--  This file was generated on Thursday 27 June 2013 at 17:35:46
--
--  $ wsdl2aws -cb -types Log4Ada ../wsdl/logging.wsdl

with Ada.Exceptions;

with SOAP.Message.Response.Error;

with Soap_Logger;

with Logging_Service.Server;
with Logging_Service.Types;

package body Logging_Service.CB is

   use Ada.Exceptions;

   pragma Warnings (Off, Logging_Service.Server);
   pragma Warnings (Off, Logging_Service.Types);

   pragma Style_Checks (Off);

   function Create_File_Log_CB is
     new Logging_Service.Server.Create_File_Log_CB (Soap_Logger.Create_File_Log);

   function Close_File_Log_CB is
     new Logging_Service.Server.Close_File_Log_CB (Soap_Logger.Close_File_Log);

   function Append_Log_CB is
     new Logging_Service.Server.Append_Log_CB (Soap_Logger.Append_Log);

   -------------
   -- SOAP_CB --
   -------------

   function SOAP_CB
     (SOAPAction : String;
      Payload    : Message.Payload.Object;
      Request    : AWS.Status.Data)
      return Response.Data is
   begin
      if SOAPAction = "Create_File_Log" then
         return Create_File_Log_CB (SOAPAction, Payload, Request);

      elsif SOAPAction = "Close_File_Log" then
         return Close_File_Log_CB (SOAPAction, Payload, Request);

      elsif SOAPAction = "Append_Log" then
         return Append_Log_CB (SOAPAction, Payload, Request);

      else
         return Message.Response.Build
           (Message.Response.Error.Build
             (Message.Response.Error.Client,
              "Wrong SOAP action " & SOAPAction));
      end if;
   exception
      when E : others =>
         return Message.Response.Build
           (Message.Response.Error.Build
             (Message.Response.Error.Client,
              "Error in SOAP_CB for SOAPAction " & SOAPAction
                & " (" & Exception_Information (E) & ")"));
   end SOAP_CB;

end Logging_Service.CB;
