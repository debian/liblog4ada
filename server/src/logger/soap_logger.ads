with Log4ada;

package Soap_Logger is

   procedure Create_File_Log (Name_ID : String);
   procedure Close_File_Log (Name_ID : String);

   procedure Append_Log (Name_ID : String;
                         Message : String;
                         Level : Log4ada.Level_Type);

end Soap_Logger;
