with GNAT.OS_Lib;

package body Logger.Environment is
   Default_Path_From_Env : GNAT.OS_Lib.String_Access := null;
begin
   Default_Path_From_Env := GNAT.OS_Lib.Getenv ("LOGGER_DIRECTORY");
   if Default_Path_From_Env.all = "" then
      Default_Path := To_Unbounded_String (".");
   else
      Default_Path := To_Unbounded_String (Default_Path_From_Env.all);
   end if;
   GNAT.OS_Lib.Free (Default_Path_From_Env);
end Logger.Environment;
