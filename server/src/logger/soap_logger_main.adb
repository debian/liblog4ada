with GNAT.Command_Line;

with AWS.Server;
with AWS.Config.Set;
with SOAP.Dispatchers.Callback;

with Log4ada;

with Logger.Appenders;
with Logging_Service.Server;
with Logging_Service.CB;

procedure Soap_Logger_Main is
   Options : constant String := "log_level= port=";
   AWS_Server : AWS.Server.HTTP;
   Config : AWS.Config.Object;
   Port : Natural := Logging_Service.Server.Port;
begin
   AWS.Config.Set.Server_Name (Config, "Soap Logging");
   AWS.Config.Set.Reuse_Address (Config, True);
   Logger.Appenders.Logger.Set_Level (Log4ada.Debug);
   Logger.Appenders.Logger.Set_Name ("soap server main");
   Logger.Appenders.Logger.Add_Appender (Logger.Appenders.Console'Access);
   loop
      case GNAT.Command_Line.Getopt (Options) is
         when ASCII.NUL =>
            exit;
         when 'l' =>
            Logger.Appenders.Logger.Set_Level
              (Log4ada.Level_Type'Value (GNAT.Command_Line.Parameter));
         when 'p' =>
            Port := Positive'Value (GNAT.Command_Line.Parameter);
         when others =>
            raise Program_Error;
      end case;
   end loop;

   Logger.Appenders.Logger.Debug_Out ("step 1");

   AWS.Config.Set.Server_Port (Config, Port);

   Logger.Appenders.Logger.Debug_Out ("step 2");

   AWS.Server.Start (AWS_Server,
                     Dispatcher => SOAP.Dispatchers.Callback.Create
                       (null,
                        Logging_Service.CB.SOAP_CB'Access),
                     Config => Config);

   Logger.Appenders.Logger.Debug_Out ("server started");

   AWS.Server.Wait;
exception
   when E : others =>
      Logger.Appenders.Logger.Error_Out ("unexpected exception", E);
end Soap_Logger_Main;
