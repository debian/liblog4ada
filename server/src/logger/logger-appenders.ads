with Log4ada.Appenders.Consoles;
with Log4ada.Loggers;

package Logger.Appenders is

   Logger : aliased Log4ada.Loggers.Logger_Type;

   Console : aliased Log4ada.Appenders.Consoles.Console_Type;

end Logger.Appenders;
