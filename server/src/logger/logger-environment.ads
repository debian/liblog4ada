with Ada.Strings.Unbounded;

package Logger.Environment is

   pragma Elaborate_Body;

   use Ada.Strings.Unbounded;

   Default_Path : Unbounded_String;

end Logger.Environment;
