------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
package body Receiver_Dictionnary is

   ---------------
   -- Subscribe --
   ---------------

   Local_Event_Receiver : Events_Receivers.Events_Receivers_Class_Access;
   pragma Atomic (Local_Event_Receiver);

   procedure Subscribe
     (Event_Receiver : Events_Receivers.Events_Receivers_Class_Access) is
   begin
      Local_Event_Receiver := Event_Receiver;
   end Subscribe;

   ------------------------
   -- Get_Event_Receiver --
   ------------------------

   function Get_Event_Receiver
     return Events_Receivers.Events_Receivers_Class_Access is
   begin
      return Local_Event_Receiver;
   end Get_Event_Receiver;

end Receiver_Dictionnary;
