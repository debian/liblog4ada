procedure Common_Output
  (Logger : not null access Log4ada.Loggers.Logger_Type;
   Exit_Loop_Log : Boolean := True) is
   use Log4ada.Loggers;
begin
   Logger.Set_Memory_Depth (6);
   loop
      Info_Out (Logger, "start demo");
      Debug_Out (Logger, "Here is some DEBUG");
      Set_Level (Logger, Log4ada.Warn);
      Info_Out (Logger, "Here is some INFO");
      Warn_Out (Logger, "Here is some WARN");
      Error_Out (Logger, "Here is some ERROR");
      delay 2.0;
      Fatal_Out (Logger, "Here is some FATAL");
      delay 1.0;
      declare
         Local_Exception : exception;
      begin
         raise Local_Exception;
      exception
         when E : others =>
            Fatal_Out (Logger, "Here is some FATAL in exception", E);
      end;
      Memory_Out (Logger, Log4ada.Debug);
      exit when Exit_Loop_Log;
   end loop;
end Common_Output;
