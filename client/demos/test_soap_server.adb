------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Log4ada.Loggers;
with Log4ada.Appenders.SOAP;

with Appenders;
with Common_Output;
with Options;

procedure Test_Soap_Server is
   use Log4ada.Loggers;
   use Appenders;
begin
   Set_Name (Logger'Access, "joli logger");
   Set_Level (Logger'Access, Log4ada.Debug);

   Add_Appender (Logger'Access, Soap_Client'Access);
   Soap_Client.Set_URL ("http://localhost:4242");
   Soap_Client.Set_ID ("test_server");

   Common_Output (Logger'Access,
                  not Options.Present_Option ("loop"));
   Soap_Client.Close;
end Test_Soap_Server;
