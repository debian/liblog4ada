------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007-2009                          --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--

with GNAT.Sockets;

with Log4ada.Appenders.Consoles;
with Log4ada.Appenders.Files;
with Log4ada.Loggers;
with Log4ada.Events;

package Tcp_Receiver is
   procedure Start (Port : GNAT.Sockets.Port_Type;
                    Name : String);
   procedure Stop;

   protected Logger_Shell is
      procedure Setup (Name : String);
      procedure Start_File_Recording (File_Name : String);
      procedure File_Name_Refresh;
      procedure Stop_File_Recording;
      procedure Start_Output;
      procedure Stop_Output;
      procedure Put_Message (Event : Log4ada.Events.Event_Type);
   private
      Setup_Done : Boolean := False;
      File_Recording_On : Boolean := False;
      Logger : aliased Log4ada.Loggers.Logger_Type;
      Console : aliased Log4ada.Appenders.Consoles.Console_Type;
      File : aliased Log4ada.Appenders.Files.File_Type;
   end Logger_Shell;

end Tcp_Receiver;
