------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Text_IO;
with GNAT.Sockets;

with Log4ada.Appenders.Consoles;
with Log4ada.Appenders.SOAP;
with Log4ada.Appenders.Files;
with Log4ada.Appenders.Tcp_Stream;
with Log4ada.Appenders.Xml;
with Log4ada.Loggers;
with Log4ada.Socket_Utilities;

package Appenders is
   Logger : aliased Log4ada.Loggers.Logger_Type;

   Console : aliased Log4ada.Appenders.Consoles.Console_Type;
   File : aliased Log4ada.Appenders.Files.File_Type;
   Stream : aliased Log4ada.Appenders.Tcp_Stream.Tcp_Stream_Type;
   Soap_Client : aliased Log4ada.Appenders.SOAP.Soap_Client_Type;

   package Appenders_Xml_Text_Io is new Log4ada.Appenders.Xml
     (Ada.Text_IO.File_Type,
      Ada.Text_IO.Standard_Output,
      Ada.Text_IO.Put,
      Ada.Text_IO.New_Line);
   Xml_Console : aliased Appenders_Xml_Text_Io.Xml_Appender_Type;

   package Appenders_Xml_Socket is new  Log4ada.Appenders.Xml
     (GNAT.Sockets.Socket_Type,
      Log4ada.Socket_Utilities.Get_Link,
      Log4ada.Socket_Utilities.Put,
      Log4ada.Socket_Utilities.New_Line);
   Xml_Socket : aliased Appenders_Xml_Socket.Xml_Appender_Type;

end Appenders;
