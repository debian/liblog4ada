------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Log4ada.Loggers;

with Appenders;
with Common_Output;
with Options;

procedure Test_Console is
   use Log4ada.Loggers;
   use Appenders;
begin
   Set_Name (Logger'Access, "joli logger");
   Set_Level (Logger'Access, Log4ada.Debug);

   Add_Appender (Logger'Access, Console'Access);
   Console.Enable_Color;

   Appenders_Xml_Text_Io.Set_Logger (Xml_Console'Access, Logger'Access);
   Add_Appender (Logger'Access, Xml_Console'Access);

   Common_Output (Logger'Access,
                  not Options.Present_Option ("loop"));
end Test_Console;
