------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007-2009                          --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--

with Ada.Exceptions;
with Tcp_Receiver;
with Options;
with GNAT.Sockets;
with Ada.Text_IO;

procedure Central_Log_Tcp is
   Port : GNAT.Sockets.Port_Type;
   function Get_Port is new Options.Get_Integer_Number (GNAT.Sockets.Port_Type);
begin
   if Options.Present_Option ("port") then
      Port := Get_Port ("port");
   else
      Port := 3334;
   end if;
   if Options.Present_Option ("name") then
      declare
         Name : constant String := Options.Get_Option ("name");
      begin
         Tcp_Receiver.Start (Port => Port,
                             Name => Name);
      end;
   else
      Tcp_Receiver.Start (Port => Port,
                          Name => "central_log_tcp");
   end if;
   declare
      Key : Character;
   begin
      loop
         Ada.Text_IO.Get_Immediate (Key);
         exit when Key = 'q' or Key = 'Q';
         case Key is
            when 'f' | 'F' =>
               Tcp_Receiver.Logger_Shell.Start_File_Recording ("test");
            when 'r' | 'R' =>
               Tcp_Receiver.Logger_Shell.File_Name_Refresh;
            when 'c' | 'C' =>
               Tcp_Receiver.Logger_Shell.Start_Output;
            when others =>
               Ada.Text_IO.Put_Line (Key & ": unknown command");
         end case;
      end loop;
   end;
   Tcp_Receiver.Stop;
exception
   when E : others =>
      Ada.Text_IO.Put_Line ("unexpected exception named : " &
                            Ada.Exceptions.Exception_Name (E));
      Ada.Text_IO.Put_Line ("with message : " &
                            Ada.Exceptions.Exception_Message (E));
end Central_Log_Tcp;
