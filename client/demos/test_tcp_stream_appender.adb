------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2009                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Appenders;

with Log4ada.Loggers;
with Log4ada.Socket_Utilities;
with Log4ada.Appenders.Tcp_Stream;

with Options;
with Common_Output;

procedure Test_Tcp_Stream_Appender is
   use Log4ada.Loggers;
   use Appenders;
begin
   Set_Name (Logger'Access, "joli logger");
   Set_Level (Logger'Access, Log4ada.Debug);

   Log4ada.Socket_Utilities.Open_Server_Link ("localhost", 3334);
   Log4ada.Appenders.Tcp_Stream.Set_Socket (Stream,
                                            Log4ada.Socket_Utilities.Get_Link);
   Add_Appender (Logger'Access, Stream'Access);

   Common_Output (Logger'Access,
                  not Options.Present_Option ("loop"));

end Test_Tcp_Stream_Appender;
