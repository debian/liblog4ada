------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Log4ada.Loggers;
with Log4ada.Appenders.Files;

with Appenders;
with Common_Output;
with Options;

procedure Test_File is
   use Log4ada.Loggers;
   use Appenders;
   function Get_Size is new
     Options.Get_Modular_Number (Log4ada.Appenders.Files.File_Size_Type);
begin
   Set_Name (Logger'Access, "joli logger");
   Set_Level (Logger'Access, Log4ada.Debug);

   if Options.Present_Option ("file_name") then
      File.Set_File_Name (Options.Get_Option ("file_name"));
   else
      File.Set_File_Name ("default_file_name.txt");
   end if;
   if Options.Present_Option ("file_max_size") then
      File.Set_File_Max_Size (Get_Size ("file_max_size"));
   else
      File.Set_File_Max_Size (16384);
   end if;
   Add_Appender (Logger'Access, File'Access);

   Common_Output (Logger'Access,
                  not Options.Present_Option ("loop"));

end Test_File;
