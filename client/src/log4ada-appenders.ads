------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Log4ada.Filters;
with Log4ada.Events;
with Log4ada.Types;

package Log4ada.Appenders is

   type Appender_Type is abstract new Types.Base_Type with private;
   type Appender_Class_Access is access all Appender_Type'Class;
   type Appender_Access is access all Appender_Type;

   procedure Add_Filter (Appender : not null access Appender_Type;
                         Filter : Filters.Filter_Class_Access);
   procedure Clear_Filters (Appender : not null access Appender_Type);
   procedure Append (Appender : not null access Appender_Type;
                     Event : Events.Event_Type) is abstract;
   procedure Disable (Appender : not null access Appender_Type);
   procedure Enable (Appender : not null access Appender_Type);
   function Enable_Status (Appender : not null access Appender_Type)
                          return Boolean;
private
   type Appender_Type is abstract new Types.Base_Type with record
      Filters_List : Filters.Vectors.Vector;
      Enabled : Boolean := True;
   end record;
   function Continue (Appender : not null access Appender_Type;
                      Event : Events.Event_Type) return Boolean;
end Log4ada.Appenders;
