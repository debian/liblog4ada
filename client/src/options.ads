------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007-2011                          --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--

package Options is

   function Present_Option (Option_Name : String) return Boolean;
   function Get_Option (Option_Name : String) return String;
   generic
      type Float_Number is digits <>;
   function Get_Float_Number (Option_Name : String)
                                     return Float_Number;
   generic
      type Integer_Number is range <>;
   function Get_Integer_Number (Option_Name : String) return Integer_Number;

   generic
      type Modular_Number is mod <>;
   function Get_Modular_Number (Option_Name : String) return Modular_Number;

   Non_Present_Option : exception;
end Options;
