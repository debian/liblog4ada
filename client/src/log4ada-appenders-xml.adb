------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Command_Line;
with GNAT.Sockets;

with McKae.XML.EZ_Out.Generic_Medium;

package body Log4ada.Appenders.Xml is

   package Output is new McKae.XML.EZ_Out.Generic_Medium (Output_Medium_Type,
                                                          Put,
                                                          New_Line);
   use Output;

   ------------
   -- Append --
   ------------

   procedure Append
     (Xml_Appender : not null access Xml_Appender_Type;
      Event : Events.Event_Type)
   is
      Level : constant Level_Type := Events.Get_Level (Event);
      Time_Stamp_Milliseconds : constant String :=
        Events.Get_Timestamp (Event)'Img & "000";
   begin
      if not Xml_Appender.Enabled then
         return;
      end if;
      if not Continue (Xml_Appender, Event) then
         return;
      end if;
      Current_Format := McKae.XML.EZ_Out.Continuous_Stream;
      Start_Element (Output_Medium,
                     "log4j:event",
                     ("logger" = Events.Get_Logger_Name (Event),
                      "timestamp" = Time_Stamp_Milliseconds
                      (2 .. Time_Stamp_Milliseconds'Last),
                      "level" = Level'Img,
                      "thread" = Events.Get_Location_Information (Event)));
      if Events.Exception_Present (Event) then
         Output_Element (Output_Medium, "log4j:message",
                         Events.Get_Message (Event) & "," &
                         Events.Get_Exception_Name (Event) & "," &
                         Events.Get_Exception_Message (Event));
      else
         Output_Element (Output_Medium, "log4j:message",
                         Events.Get_Message (Event));
      end if;
      Start_Element (Output_Medium, "log4j:NDC");
      Output_Element (Output_Medium, "host", GNAT.Sockets.Host_Name);
      Output_Element (Output_Medium,
                      "prog_name",
                      Ada.Command_Line.Command_Name);
      End_Element (Output_Medium, "log4j:NDC");
      Start_Element (Output_Medium, "log4j:throwable");
      End_Element (Output_Medium, "log4j:throwable");
      End_Element (Output_Medium, "log4j:event");
   end Append;

   procedure Set_Logger (Xml_Appender : not null access Xml_Appender_Type;
                         Logger : Loggers.Logger_Class_Access) is
   begin
      Xml_Appender.Logger := Logger;
   end Set_Logger;

end Log4ada.Appenders.Xml;
