------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2011                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Text_IO;

package body Log4ada.Appenders.Consoles is

   procedure Change_Display_Mode (Level : Level_Type := Debug);
   procedure Raz_Display_Mode;

   procedure Change_Display_Mode (Level : Level_Type := Debug) is
   begin
      case Level is
         when All_Level | Off =>
            null;
         when Debug =>
            Ada.Text_IO.Put (ASCII.ESC & "[1;32m");
         when Info =>
            Ada.Text_IO.Put (ASCII.ESC & "[1;34m");
         when Warn =>
            Ada.Text_IO.Put (ASCII.ESC & "[1;33m");
         when Error =>
            Ada.Text_IO.Put (ASCII.ESC & "[1;31m");
         when Fatal =>
            Ada.Text_IO.Put (ASCII.ESC & "[5;1;31m");
      end case;
   end Change_Display_Mode;

   procedure Raz_Display_Mode is
   begin
      Ada.Text_IO.Put (ASCII.ESC & "[0;m");
   end Raz_Display_Mode;

   ------------
   -- Append --
   ------------

   procedure Append
     (Console : not null access Console_Type;
      Event : Events.Event_Type)
   is
      Timestamp_Diff : Integer;
      use Events;
   begin
      if not Console.Enabled then
         return;
      end if;
      if not Continue (Console, Event) then
         return;
      end if;
      Timestamp_Diff := Get_Timestamp (Event) - Events.First_Event_Timestamp;
      if Console.Display_Color then
         Change_Display_Mode (Get_Level (Event));
      end if;
      Ada.Text_IO.Put (Timestamp_Diff'Img & " ");
      Ada.Text_IO.Put (Get_Level (Event)'Img & " [");
      Ada.Text_IO.Put (Get_Location_Information (Event) & "] ");
      Ada.Text_IO.Put (Get_Logger_Name (Event) & " - ");
      Ada.Text_IO.Put (Get_Message (Event));
      if Exception_Present (Event) then
         Ada.Text_IO.Put (" - " & Get_Exception_Name (Event));
         Ada.Text_IO.Put_Line (" - " & Get_Exception_Message (Event));
      else
         Ada.Text_IO.New_Line;
      end if;
      if Console.Display_Color then
         Raz_Display_Mode;
      end if;
   end Append;

   procedure Enable_Color (Console : not null access Console_Type) is
   begin
      Console.Display_Color := True;
   end Enable_Color;

   procedure Disable_Color (Console : not null access Console_Type) is
   begin
      Console.Display_Color := False;
   end Disable_Color;

end Log4ada.Appenders.Consoles;
