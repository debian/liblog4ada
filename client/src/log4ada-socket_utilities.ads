------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Text_IO;
with GNAT.Sockets;

package Log4ada.Socket_Utilities is
   procedure Open_Server_Link (Host : String;
                               Port : GNAT.Sockets.Port_Type);
   function Get_Link return GNAT.Sockets.Socket_Type;
   procedure Put (Socket : in GNAT.Sockets.Socket_Type;
                  S : String);
   procedure New_Line (Socket : in GNAT.Sockets.Socket_Type;
                       Spacing : in Ada.Text_IO.Positive_Count := 1);
   procedure Close_Server_Link (Socket : GNAT.Sockets.Socket_Type)
     renames GNAT.Sockets.Close_Socket;
end Log4ada.Socket_Utilities;
