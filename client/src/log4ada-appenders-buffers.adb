package body Log4ada.Appenders.Buffers is

   -----------
   -- Clear --
   -----------

   procedure Clear (Buffer : not null access Buffer_Type) is
   begin
      Buffer.Log_Repo.Clear;
   end Clear;

   ---------------
   -- Set_Depth --
   ---------------

   procedure Set_Depth
     (Buffer : not null access Buffer_Type;
      Depth : Natural)
   is
   begin
      Buffer.Log_Repo.Set_Depth (Depth);
   end Set_Depth;

   --------------
   -- Get_Logs --
   --------------

   function Get_Logs
     (Buffer : not null access Buffer_Type)
      return Types.String_Vectors.Vector
   is
   begin
      return Buffer.Log_Repo.Get_Logs;
   end Get_Logs;

   ------------
   -- Append --
   ------------

   procedure Append
     (Buffer : not null access Buffer_Type;
      Event : Events.Event_Type)
   is
      Timestamp_Diff : Integer;
      use Events;
   begin
      Timestamp_Diff := Get_Timestamp (Event) - Events.First_Event_Timestamp;
      declare
         Log_Message_Base : constant String :=
           Timestamp_Diff'Img & " " &
           Get_Level (Event)'Img & " [" &
           Get_Location_Information (Event) & "] " &
           Get_Logger_Name (Event) & " - " &
           Get_Message (Event);
         End_Of_Message : constant String :=
           (if Exception_Present (Event) then
               " - " & Get_Exception_Name (Event) & " - " &
               Get_Exception_Message (Event)
            else "");
      begin
         Buffer.Log_Repo.Append (Log_Message_Base & End_Of_Message);
      end;
   end Append;

   ----------------
   -- Logs_Shell --
   ----------------

   protected body Logs_Shell is

      ---------------
      -- Set_Depth --
      ---------------

      procedure Set_Depth (New_Depth : Natural) is
      begin
         Depth := New_Depth;
         if New_Depth /= 0 then
            loop
               exit when Natural (Logs.Length) <= New_Depth;
               Logs.Delete_First;
            end loop;
         end if;
      end Set_Depth;

      ------------
      -- Append --
      ------------

      procedure Append (Log : String) is
      begin
         Logs.Append (Log);
         if Depth /= 0 and Natural (Logs.Length) > Depth then
            Logs.Delete_First;
         end if;
      end Append;

      -----------
      -- Clear --
      -----------

      procedure Clear is
      begin
         Logs.Clear;
      end Clear;

      --------------
      -- Get_Logs --
      --------------

      function Get_Logs return Types.String_Vectors.Vector is
      begin
         return Logs;
      end Get_Logs;

   end Logs_Shell;

end Log4ada.Appenders.Buffers;
