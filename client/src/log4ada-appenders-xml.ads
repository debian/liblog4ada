------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Text_IO;
with Log4ada.Loggers;

generic
   type Output_Medium_Type is limited private;
   with function Output_Medium return Output_Medium_Type;
   with procedure Put (F : in Output_Medium_Type; S : in String) is <>;
   with procedure New_Line
     (F       : in Output_Medium_Type;
      Spacing : in Ada.Text_IO.Positive_Count := 1) is <>;
package Log4ada.Appenders.Xml is

   type Xml_Appender_Type is new Appender_Type with private;
   procedure Append (Xml_Appender : not null access Xml_Appender_Type;
                     Event : Events.Event_Type);
   procedure Set_Logger (Xml_Appender : not null access Xml_Appender_Type;
                         Logger : Loggers.Logger_Class_Access);
private
   type Xml_Appender_Type is new Appender_Type with record
      Logger : Loggers.Logger_Class_Access := null;
   end record;
end Log4ada.Appenders.Xml;
