------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2009                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Strings.Unbounded;
with Ada.Text_IO;
with Interfaces;

package Log4ada.Appenders.Files is
   type File_Type is new Appender_Type with private;
   type File_Class_Access is access all File_Type'Class;
   type File_Access is access all File_Type;

   procedure Open (File : not null access File_Type;
                   Name : String);
   procedure Refresh (File : not null access File_Type);
   procedure Close (File : not null access File_Type);
   procedure Append (File : not null access File_Type;
                     Event : Events.Event_Type);
   procedure Set_File_Name (File : not null access File_Type;
                            Name : String) renames Open;
   type File_Size_Type is new Interfaces.Unsigned_64;
   procedure Set_File_Max_Size (File : not null access File_Type;
                                Size : File_Size_Type);

private
   type File_Type is new Appender_Type with record
      File_Open : Boolean := False;
      Current_Number : Natural := 0;
      File_Name : Ada.Strings.Unbounded.Unbounded_String;
      File : Ada.Text_IO.File_Type;
      Max_Size : File_Size_Type := File_Size_Type'Last;
      Current_Size : File_Size_Type := 0;
   end record;

end Log4ada.Appenders.Files;
