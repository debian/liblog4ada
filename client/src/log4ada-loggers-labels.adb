package body Log4ada.Loggers.Labels is

   procedure Set_Threshold (Logger : not null access Label_Logger_Type;
                            Label : String;
                            Level : Level_Type) is
   begin
      if Logger.Thresholds.Contains (Label) then
         Logger.Thresholds.Replace (Label, Level);
      else
         Logger.Thresholds.Insert (Label, Level);
      end if;
   end Set_Threshold;

   procedure Set_Default_Threshold (Logger : not null access Label_Logger_Type;
                                    Level : Level_Type) is
   begin
      Logger.Set_Threshold ("default_level", Level);
   end Set_Default_Threshold;

   function Get_Threshold (Logger : not null access Label_Logger_Type;
                           Label : String) return Log4ada.Level_Type is
   begin
      if Logger.Thresholds.Contains (Label) then
         return Logger.Thresholds.Element (Label);
      elsif Logger.Thresholds.Contains ("default_level") then
         return Logger.Thresholds.Element ("default_level");
      else
         Logger.Debug_Out ("LOG4ADA : " & Label & " : this label/logger has no defined default log_level => use info");
         return Info;
      end if;
   end Get_Threshold;

   function Get_Thresholds (Logger : not null access Label_Logger_Type) return Threshold_Maps.Map is
   begin
      return Logger.Thresholds;
   end Get_Thresholds;

   ---------------
   -- Debug_Out --
   ---------------

   procedure Debug_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True)
   is
   begin
      if Logger.Is_Right_Label (Label, Debug) then
         Logger.Debug_Out (Label & " : " & Message, Exception_To_Send, With_Traces);
      end if;
   end Debug_Out;

   --------------
   -- Info_Out --
   --------------

   procedure Info_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True)
   is
   begin
      if Logger.Is_Right_Label (Label, Info) then
         Logger.Info_Out (Label & " : " & Message, Exception_To_Send, With_Traces);
      end if;
   end Info_Out;

   --------------
   -- Warn_Out --
   --------------

   procedure Warn_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True)
   is
   begin
      if Logger.Is_Right_Label (Label, Warn) then
         Logger.Warn_Out (Label & " : " & Message, Exception_To_Send, With_Traces);
      end if;
   end Warn_Out;

   ---------------
   -- Error_Out --
   ---------------

   procedure Error_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True)
   is
   begin
      if Logger.Is_Right_Label (Label, Error) then
         Logger.Error_Out (Label & " : " & Message, Exception_To_Send, With_Traces);
      end if;
   end Error_Out;

   ---------------
   -- Fatal_Out --
   ---------------

   procedure Fatal_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True)
   is
   begin
      if Logger.Is_Right_Label (Label, Fatal) then
         Logger.Fatal_Out (Label & " : " & Message, Exception_To_Send, With_Traces);
      end if;
   end Fatal_Out;

   ---------------
   -- Debug_Out --
   ---------------

   procedure Debug_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String)
   is
   begin
      if Logger.Is_Right_Label (Label, Debug) then
         Logger.Debug_Out (Label & " : " & Message);
      end if;
   end Debug_Out;

   --------------
   -- Info_Out --
   --------------

   procedure Info_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String)
   is
   begin
      if Logger.Is_Right_Label (Label, Info) then
         Logger.Info_Out (Label & " : " & Message);
      end if;
   end Info_Out;

   --------------
   -- Warn_Out --
   --------------

   procedure Warn_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String)
   is
   begin
      if Logger.Is_Right_Label (Label, Warn) then
         Logger.Warn_Out (Label & " : " & Message);
      end if;
   end Warn_Out;

   ---------------
   -- Error_Out --
   ---------------

   procedure Error_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String)
   is
   begin
      if Logger.Is_Right_Label (Label, Error) then
         Logger.Error_Out (Label & " : " & Message);
      end if;
   end Error_Out;

   ---------------
   -- Fatal_Out --
   ---------------

   procedure Fatal_Out
     (Logger : not null access Label_Logger_Type;
      Label : String;
      Message : String)
   is
   begin
      if Logger.Is_Right_Label (Label, Fatal) then
         Logger.Fatal_Out (Label & " : " & Message);
      end if;
   end Fatal_Out;

   -----------------
   -- Enum_Logger --
   -----------------

   package body Enum_Logger is

      ---------------
      -- Debug_Out --
      ---------------

      procedure Debug_Out (Kind : Enum_Type;
                           Message : String;
                           Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                           With_Traces : Boolean := True) is
      begin
         Logger.Debug_Out (Kind'Img, Message, Exception_To_Send, With_Traces);
      end Debug_Out;

      --------------
      -- Info_Out --
      --------------

      procedure Info_Out (Kind : Enum_Type;
                          Message : String;
                          Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                          With_Traces : Boolean := True) is
      begin
         Logger.Info_Out (Kind'Img, Message, Exception_To_Send, With_Traces);
      end Info_Out;

      --------------
      -- Warn_Out --
      --------------

      procedure Warn_Out (Kind : Enum_Type;
                          Message : String;
                          Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                          With_Traces : Boolean := True) is
      begin
         Logger.Warn_Out (Kind'Img, Message, Exception_To_Send, With_Traces);
      end Warn_Out;

      ---------------
      -- Error_Out --
      ---------------

      procedure Error_Out (Kind : Enum_Type;
                           Message : String;
                           Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                           With_Traces : Boolean := True) is
      begin
         Logger.Error_Out (Kind'Img, Message, Exception_To_Send, With_Traces);
      end Error_Out;

      ---------------
      -- Fatal_Out --
      ---------------

      procedure Fatal_Out (Kind : Enum_Type;
                           Message : String;
                           Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                           With_Traces : Boolean := True) is
      begin
         Logger.Fatal_Out (Kind'Img, Message, Exception_To_Send, With_Traces);
      end Fatal_Out;

      ---------------
      -- Debug_Out --
      ---------------

      procedure Debug_Out (Kind : Enum_Type;
                           Message : String) is
      begin
         Logger.Debug_Out (Kind'Img, Message);
      end Debug_Out;

      --------------
      -- Info_Out --
      --------------

      procedure Info_Out (Kind : Enum_Type;
                          Message : String) is
      begin
         Logger.Info_Out (Kind'Img, Message);
      end Info_Out;

      --------------
      -- Warn_Out --
      --------------

      procedure Warn_Out (Kind : Enum_Type;
                          Message : String) is
      begin
         Logger.Warn_Out (Kind'Img, Message);
      end Warn_Out;

      ---------------
      -- Error_Out --
      ---------------

      procedure Error_Out (Kind : Enum_Type;
                           Message : String) is
      begin
         Logger.Error_Out (Kind'Img, Message);
      end Error_Out;

      ---------------
      -- Fatal_Out --
      ---------------

      procedure Fatal_Out (Kind : Enum_Type;
                           Message : String) is
      begin
         Logger.Fatal_Out (Kind'Img, Message);
      end Fatal_Out;

      -------------------
      -- Set_Threshold --
      -------------------

      procedure Set_Threshold (Kind : Enum_Type;
                               Level : Level_Type) is
      begin
         Logger.Set_Threshold (Kind'Img, Level);
      end Set_Threshold;

      function Get_Threshold (Kind : Enum_Type) return Level_Type is
      begin
         return Logger.Get_Threshold (Kind'Img);
      end Get_Threshold;
   begin
      for Enum in Enum_Type loop
         Logger.Set_Threshold (Enum'Img, Warn);
      end loop;
   end Enum_Logger;

end Log4ada.Loggers.Labels;
