------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2015                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

with Log4ada.Types;

package Log4ada.Appenders.Buffers is

   type Buffer_Type is new Appender_Type with private;
   type Buffer_Class_Access is access all Buffer_Type'Class;
   type Buffer_Access is access all Buffer_Type;

   procedure Clear (Buffer : not null access Buffer_Type);
   --  depth 0 => no limit
   procedure Set_Depth (Buffer : not null access Buffer_Type;
                        Depth : Natural);
   function Get_Logs (Buffer : not null access Buffer_Type)
                     return Types.String_Vectors.Vector;
   overriding procedure Append (Buffer : not null access Buffer_Type;
                                Event : Events.Event_Type);

private

   protected type Logs_Shell is
      procedure Set_Depth (New_Depth : Natural);
      procedure Append (Log : String);
      procedure Clear;
      function Get_Logs return Types.String_Vectors.Vector;
   private
      Logs : Types.String_Vectors.Vector;
      Depth : Natural := 0;
   end Logs_Shell;

   type Buffer_Type is new Appender_Type with record
      Log_Repo : Logs_Shell;
   end record;

end Log4ada.Appenders.Buffers;
