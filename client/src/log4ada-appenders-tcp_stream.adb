------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2009                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

package body Log4ada.Appenders.Tcp_Stream is

   ------------
   -- Append --
   ------------

   procedure Append
     (Tcp_Stream : not null access Tcp_Stream_Type;
      Event : Events.Event_Type)
   is
      Channel : GNAT.Sockets.Stream_Access :=
        GNAT.Sockets.Stream (Tcp_Stream.Socket);
   begin
      if not Tcp_Stream.Socket_Open then
         return;
      end if;
      Events.Event_Type'Output (Channel, Event);
      GNAT.Sockets.Free (Channel);
   end Append;

   procedure Set_Socket (Tcp_Stream : in out Tcp_Stream_Type;
                         Socket : GNAT.Sockets.Socket_Type) is
   begin
      Tcp_Stream.Socket := Set_Socket.Socket;
      Tcp_Stream.Socket_Open := True;
   end Set_Socket;

end Log4ada.Appenders.Tcp_Stream;
