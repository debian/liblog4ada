------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007-2009                          --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--

with Ada.Command_Line;

package body Options is
   function Present_Option (Option_Name : String) return Boolean is
      Extended_Option : constant String := "--" & Option_Name;
   begin
      for I in 1 .. Ada.Command_Line.Argument_Count loop
         if Ada.Command_Line.Argument (I) = Extended_Option then
            return True;
         end if;
      end loop;
      return False;
   end Present_Option;

   function Get_Option (Option_Name : String) return String is
      Extended_Option : constant String := "--" & Option_Name;
   begin
      for I in 1 .. Ada.Command_Line.Argument_Count loop
         if Ada.Command_Line.Argument (I) = Extended_Option then
            if I < Ada.Command_Line.Argument_Count then
               return Ada.Command_Line.Argument (I + 1);
            else
               raise Non_Present_Option with Option_Name & ": not present";
            end if;
         end if;
      end loop;
      raise Non_Present_Option with Option_Name & ": not present";
   end Get_Option;

   function Get_Float_Number (Option_Name : String)
                                     return Float_Number is
   begin
      return Float_Number'Value (Get_Option (Option_Name));
   end Get_Float_Number;

   function Get_Integer_Number (Option_Name : String)
                                    return Integer_Number is
   begin
      return Integer_Number'Value (Get_Option (Option_Name));
   end Get_Integer_Number;

   function Get_Modular_Number (Option_Name : String) return Modular_Number is
   begin
      return Modular_Number'Value (Get_Option (Option_Name));
   end Get_Modular_Number;

end Options;
