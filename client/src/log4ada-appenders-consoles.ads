------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2011                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
package Log4ada.Appenders.Consoles is
   type Console_Type is new Appender_Type with private;
   procedure Append (Console : not null access Console_Type;
                     Event : Events.Event_Type);
   procedure Enable_Color (Console : not null access Console_Type);
   procedure Disable_Color (Console : not null access Console_Type);
private
   type Console_Type is new Appender_Type with record
      Display_Color : Boolean := False;
   end record;
end Log4ada.Appenders.Consoles;
