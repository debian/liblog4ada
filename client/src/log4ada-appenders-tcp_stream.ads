------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                          Copyright (C) 2007-2009                         --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --

with GNAT.Sockets;

package Log4ada.Appenders.Tcp_Stream is
   type Tcp_Stream_Type is limited new Appender_Type with private;
   procedure Append (Tcp_Stream : not null access Tcp_Stream_Type;
                     Event : Events.Event_Type);
   procedure Set_Socket (Tcp_Stream : in out Tcp_Stream_Type;
                         Socket : GNAT.Sockets.Socket_Type);
private
   type Tcp_Stream_Type is limited new Appender_Type with record
      Socket_Open : Boolean := False;
      Socket : GNAT.Sockets.Socket_Type;
   end record;
end Log4ada.Appenders.Tcp_Stream;
