------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
package body Log4ada.Appenders is

   use Filters.Vectors;

   ----------------
   -- Add_Filter --
   ----------------

   procedure Add_Filter
     (Appender : not null access Appender_Type;
      Filter : Filters.Filter_Class_Access)
   is
   begin
      Append (Appender.Filters_List, Filter);
   end Add_Filter;

   -------------------
   -- Clear_Filters --
   -------------------

   procedure Clear_Filters (Appender : not null access Appender_Type) is
   begin
      loop
         exit when Is_Empty (Appender.Filters_List);
         Delete_Last (Appender.Filters_List);
      end loop;
   end Clear_Filters;

   procedure Disable (Appender : not null access Appender_Type) is
   begin
      Appender.Enabled := False;
   end Disable;

   procedure Enable (Appender : not null access Appender_Type) is
   begin
      Appender.Enabled := True;
   end Enable;

   function Enable_Status (Appender : not null access Appender_Type)
                          return Boolean is
   begin
      return Appender.Enabled;
   end Enable_Status;

   function Continue (Appender : not null access Appender_Type;
                      Event : Events.Event_Type)
                     return Boolean is
      List_Length : Natural;
      Index : Positive;
      Decide_Result : Filters.Decision_Type;
      use type Filters.Decision_Type;
      Filter : Filters.Filter_Class_Access;
   begin
      if not Is_Empty (Appender.Filters_List) then
         Index := First_Index (Appender.Filters_List);
         List_Length := Natural (Length (Appender.Filters_List));
         for I in Index .. Index + List_Length - 1 loop
            Filter := Element (Appender.Filters_List, I);
            Decide_Result := Filters.Decide (Filter, Event);
            exit when Decide_Result = Filters.Accept_Event;
            if Decide_Result = Filters.Deny then
               return False;
            end if;
         end loop;
      end if;
      return True;
   end Continue;

end Log4ada.Appenders;
