------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                         Copyright (C) 2007 - 2009                        --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Strings.Unbounded;
with Ada.Exceptions;
with Ada.Unchecked_Deallocation;
with Ada.Containers.Vectors;

package Log4ada.Events is
   type Event_Type is private;
   type Event_Access is access all Event_Type;
   procedure Free (Event : in out Event_Access);
   package Event_Vectors is new Ada.Containers.Vectors (Positive,
                                                        Event_Access,
                                                        "=");

   function New_Event (Location_Information : String;
                       Logger_Name : String;
                       Level : Level_Type;
                       Timestamp : Integer;
                       Message : String;
                       Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
                       With_Traces : Boolean := True)
                      return Event_Type;
   function New_Event (Location_Information : String;
                       Logger_Name : String;
                       Level : Level_Type;
                       Timestamp : Integer;
                       Message : String)
                      return Event_Type;
   pragma Inline (New_Event);
   function Get_Location_Information (Event : Event_Type) return String;
   function Get_Logger_Name (Event : Event_Type) return String;
   function Get_Level (Event : Event_Type) return Level_Type;
   function Get_Timestamp (Event : Event_Type) return Integer;
   function Get_Message (Event : Event_Type) return String;
   function Exception_Present (Event : Event_Type) return Boolean;
   function Get_Exception_Name (Event : Event_Type) return String;
   function Get_Exception_Message (Event : Event_Type) return String;
   function Get_Trace (Event : Event_Type) return String;
   First_Event_Timestamp : Integer := 0;
   pragma Atomic (First_Event_Timestamp);
   function To_Event (Event_String : String) return Event_Type;
   function To_String (Event : Event_Type) return String;
private
   type Event_Type is tagged record
      Name : Ada.Strings.Unbounded.Unbounded_String;
      Location_Information : Ada.Strings.Unbounded.Unbounded_String;
      Level : Level_Type := Off;
      Timestamp : Integer := 0;
      Message : Ada.Strings.Unbounded.Unbounded_String;
      Logger_Name : Ada.Strings.Unbounded.Unbounded_String;
      Exception_Present : Boolean := False;
      Exception_To_Send_Name : Ada.Strings.Unbounded.Unbounded_String;
      Exception_To_Send_Message : Ada.Strings.Unbounded.Unbounded_String;
      With_Traces : Boolean;
      Trace : Ada.Strings.Unbounded.Unbounded_String;
   end record;
   procedure Real_Free is new Ada.Unchecked_Deallocation (Event_Type,
                                                          Event_Access);
end Log4ada.Events;
