------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Containers.Vectors;

with Log4ada.Events;
with Log4ada.Types;

package Log4ada.Filters is
   type Decision_Type is (Accept_Event, Neutral, Deny);
   type Filter_Type is abstract new Types.Base_Type with private;
   subtype Filter_Class is Filter_Type'Class;
   type Filter_Access is access all Filter_Type;
   type Filter_Class_Access is access all Filter_Type'Class;
   function Decide (Filter : not null access Filter_Type;
                    Event : Events.Event_Type)
                   return Decision_Type is abstract;
   package Vectors is new Ada.Containers.Vectors (Positive,
                                                  Filter_Class_Access);
private
   type Filter_Type is abstract new Types.Base_Type with null record;
end Log4ada.Filters;
