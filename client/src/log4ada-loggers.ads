------------------------------------------------------------------------------
--                                  Log4Ada                                 --
--                                                                          --
--                            Copyright (C) 2007                            --
--                              X. Grave CNRS                               --
--                                                                          --
--  This library is free software; you can redistribute it and/or modify    --
--  it under the terms of the GNU General Public License as published by    --
--  the Free Software Foundation; either version 2 of the License, or (at   --
--  your option) any later version.                                         --
--                                                                          --
--  This library is distributed in the hope that it will be useful, but     --
--  WITHOUT ANY WARRANTY; without even the implied warranty of              --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       --
--  General Public License for more details.                                --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this library; if not, write to the Free Software Foundation, --
--  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.          --
--                                                                          --
with Ada.Containers.Vectors;
with Ada.Exceptions;

with Log4ada.Appenders;
with Log4ada.Events;
with Log4ada.Types;

package Log4ada.Loggers is
   type Logger_Type is new Types.Base_Type with private;
   type Logger_Class_Access is access all Logger_Type'Class;
   type Logger_Access is access all Logger_Type;

   procedure Set_Level (Logger : not null access Logger_Type;
                        New_Level : Level_Type);
   function Get_Level (Logger : not null access Logger_Type) return Level_Type;
   procedure Add_Appender (Logger : not null access Logger_Type;
                           Appender : Appenders.Appender_Class_Access);
   procedure Event_Out (Logger : not null access Logger_Type;
                        Event : Events.Event_Type);
   procedure Set_Memory_Depth (Logger : not null access Logger_Type;
                               Depth : Natural);
   procedure Debug_Out
     (Logger : not null access Logger_Type;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);
   procedure Info_Out
     (Logger : not null access Logger_Type;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);
   procedure Warn_Out
     (Logger : not null access Logger_Type;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);
   procedure Error_Out
     (Logger : not null access Logger_Type;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);
   procedure Fatal_Out
     (Logger : not null access Logger_Type;
      Message : String;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);

   procedure Debug_Out (Logger : not null access Logger_Type;
                        Message : String);
   procedure Info_Out (Logger : not null access Logger_Type;
                       Message : String);
   procedure Warn_Out (Logger : not null access Logger_Type;
                       Message : String);
   procedure Error_Out (Logger : not null access Logger_Type;
                        Message : String);
   procedure Fatal_Out (Logger : not null access Logger_Type;
                        Message : String);
   procedure Memory_Out (Logger : not null access Logger_Type;
                         Level : Level_Type);
   No_Appender : exception;
   procedure Logger_Output
     (Logger : not null access Logger_Type;
      Message : String;
      Level : Level_Type;
      Exception_To_Send : Ada.Exceptions.Exception_Occurrence;
      With_Traces : Boolean := True);
   procedure Logger_Output
     (Logger : not null access Logger_Type;
      Message : String;
      Level : Level_Type);
   procedure Set_Lock_Delay (Logger : not null access Logger_Type;
                             Lock_Delay : Duration);

private
   package Appenders_Vector is new Ada.Containers.Vectors
     (Positive, Appenders.Appender_Class_Access, Appenders."=");
   use Appenders_Vector;
   protected type Lock_Type is
      entry Get;
      procedure Release;
   private
      Available : Boolean := True;
   end Lock_Type;
   type Logger_Type is new Types.Base_Type with record
      Current_Level : Level_Type := Off;
      Appenders_List : Vector;
      Lock : Lock_Type;
      Lock_Delay : Duration := 1.0;
      Depth : Natural := 1;
      Events_Vector : Events.Event_Vectors.Vector;
   end record;
end Log4ada.Loggers;
