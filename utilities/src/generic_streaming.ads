with Ada.Streams; use Ada.Streams;
with Ada.Unchecked_Deallocation;

package Generic_Streaming is
   type Stream_Element_Array_Access is access Stream_Element_Array;
   procedure Free is new Ada.Unchecked_Deallocation
     (Stream_Element_Array, Stream_Element_Array_Access);
   type Basic_Stream is new Root_Stream_Type with record
      Data : Stream_Element_Array_Access := null;
      Cursor : Stream_Element_Offset;
   end record;

   procedure Read
     (Stream : in out Basic_Stream;
      Item   : out Stream_Element_Array;
      Last   : out Stream_Element_Offset);

   procedure Write
     (Stream : in out Basic_Stream;
      Item   : Stream_Element_Array);

end Generic_Streaming;
