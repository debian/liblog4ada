package body Generic_Streaming is

   ----------
   -- Read --
   ----------

   procedure Read
     (Stream : in out Basic_Stream;
      Item   : out Stream_Element_Array;
      Last   : out Stream_Element_Offset)
   is
   begin
      Item := Stream.Data.all
        (Stream.Cursor .. Stream.Cursor + Item'Length - 1);
      Last := Item'Length;
      Stream.Cursor := Stream.Cursor + Item'Length;
      if Stream.Cursor > Stream.Data.all'Last then
         Free (Stream.Data);
      end if;
   end Read;

   -----------
   -- Write --
   -----------

   procedure Write
     (Stream : in out Basic_Stream;
      Item   : Stream_Element_Array)
   is
   begin
      if Stream.Data = null then
         Stream.Data := new Stream_Element_Array (1 .. Item'Length);
         Stream.Data.all := Item;
      else
         declare
            Temp : constant Stream_Element_Array := Stream.Data.all;
         begin
            Free (Stream.Data);
            Stream.Data := new Stream_Element_Array
              (1 .. Temp'Length + Item'Length);
            Stream.Data.all (1 .. Temp'Length) := Temp;
            Stream.Data.all (Temp'Length + 1 .. Temp'Length + Item'Length) :=
              Item;
         end;
      end if;
      Stream.Cursor := 1;
   end Write;

end Generic_Streaming;
